from chat_app_lib import Machine

LOCAL_HOST = '127.0.0.1'
PORT1 = 8088
PORT2 = 8089
PORT3 = 8090

peer1 = Machine()
peer1.name = "Machine 1"
peer1.host = LOCAL_HOST
peer1.port = PORT1

peer2 = Machine()
peer2.name = "Machine 2"
peer2.host = LOCAL_HOST
peer2.port = PORT2

peer3 = Machine()
peer3.name = "Machine 3"
peer3.host = LOCAL_HOST
peer3.port = PORT3