from socket import *
import thread


class Machine:
    name = ""
    host = ""
    port = 0
    broadcast_socket = None
    peers = []


def runServer(machine):
    # run UDP broadcast server
    thread.start_new_thread(server_UDP_broadcast, (machine,))

    # run P2P server
    s = socket(AF_INET, SOCK_STREAM)
    s.bind((machine.host, machine.port))
    s.listen(1)

    print "Server is listening on Port: " + str(machine.port) + " - IP address : " + machine.host + "\n"
    thread.start_new_thread(server_accept_P2Pclient, (s,))
    thread.start_new_thread(server_accept_P2Pclient, (s,))


def server_accept_P2Pclient(s):
    conn, addr = s.accept()
    print addr[0], ":", addr[1], "\n"

    while True:
        try:
            machineName = conn.recv(1024)
            message = conn.recv(1024)
            if (message == "" or message == "exit"):
                conn.close()
                break
            print machineName + ": " + message
        except:
            conn.close()
            break


def initiateP2PClient(machine):
    succeeded = False
    client = socket(AF_INET, SOCK_STREAM)
    while not succeeded:
        try:
            client.connect((machine.host, machine.port))
            succeeded = True
        except:
            succeeded = False
    return client


def choosePeer(peers):
    input = -1
    inputValid = False
    while not inputValid:
        count = 1
        for peer in peers:
            print "[" + str(count) + "] " + peer.name
            count += 1

        print "[" + str(count) + "] Broadcast"
        print "Enter option number: "
        input = int(raw_input())
        if input > 0 and input <= count:
            inputValid = True
    return (input - 1)


def runClient(clientMachine):
    # Making P2P connections to other machines
    socketObjects = []
    for peer in clientMachine.peers:
        socketObjects.append(initiateP2PClient(peer))

    peerIndex = choosePeer(clientMachine.peers)
    print "Enter your message: "
    message = raw_input()

    while True:
        if (peerIndex == len(socketObjects)):
            broadcast_message(clientMachine, message)

        if (peerIndex != -1):
            socketObjects[peerIndex].send(clientMachine.name)
            socketObjects[peerIndex].send(message)

        if message == "" or message == "exit":
            socketObjects[peerIndex].close()
            break

        peerIndex = choosePeer(clientMachine.peers)
        print "Enter your message: "
        message = raw_input()


### Broadcast UDP code ##

broadcast_addr = ""

def server_UDP_broadcast(machine):
    machine.broadcast_socket = socket(AF_INET, SOCK_DGRAM)
    broadcast_addr = get_broadcast_address(machine.host)
    serverSocket = socket(AF_INET, SOCK_DGRAM)
    serverSocket.bind((broadcast_addr, machine.port))
    while True:
        message, address = serverSocket.recvfrom(1024)
        print "[*] Receved'", message, "' From ", address[0], ":", address[1]

def broadcast_message(machine, message):
    machine.broadcast_socket.sendto(message, (broadcast_addr, machine.port))

###  getting broadcast address
# hostname = socket.gethostname()
# myIPAddr = socket.gethostbyname(hostname)

def get_broadcast_address(ip):
    mask = '255.255.0.0'
    ip = ip.split('.')
    mask = mask.split('.')
    ip = [int(bin(int(octet)), 2) for octet in ip]
    mask = [int(bin(int(octet)), 2) for octet in mask]
    subnet = [str(int(bin(ioctet & moctet), 2)) for ioctet, moctet in zip(ip, mask)]
    host = [str(int(bin(ioctet & ~moctet), 2)) for ioctet, moctet in zip(ip, mask)]
    broadcast = [str(int(bin(ioctet | ~moctet), 2) & 0xff) for ioctet, moctet in zip(ip, mask)]  # a mistake, i guess
    # print 'Subnet: {0}'.format('.'.join(subnet))
    # print 'Host: {0}'.format('.'.join(host))
    # print 'Broadcast address: {0}'.format('.'.join(broadcast))
    broadcast_string = ""
    for i in range(len(broadcast)):
        broadcast_string += broadcast[i]
        if i < (len(broadcast)-1):
            broadcast_string += '.'

    # return broadcast_string
    return '192.168.0.182'
